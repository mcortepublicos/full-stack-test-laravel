import Posts from "./components/Posts";
import Post from "./components/Post";
import App from "./App.vue";

export const routes = [
    {
        name: 'posts',
        path: '/',
        component: Posts
    },
    {
        name: 'post',
        path: '/post/:id',
        component: Post
    },
];
