@extends('layouts.app')

@section('content')
<div class="container">
    <div class="content p-1">
        <div class="list-group-item">
            <div class="d-flex">
                <div class="mr-auto p-2">
                    <h2 class="display-8 titulo">Dashboard</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 mb-3">
                    <div class="card bg-success text-white">
                        <div class="card-body">
                            <i class="fas fa-users fa-3x"></i>
                            <h6 class="card-title">Usuários cadastrados</h6>
                            <h2 class="lead">{{$users}}</h2>
                            <span href="/admin/posts" class="card-link text-white">Total de usuários</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-3">
                    <div class="card bg-danger text-white">
                        <div class="card-body" onclick="/admin/posts">
                            <i class="fas fa-file fa-3x"></i>
                            <h6 class="card-title">Posts Publicados</h6>
                            <h2 class="lead">{{$publicados}}</h2>
                            <a href="/admin/posts" class="card-link text-white">Acessar posts</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 mb-3">
                    <div class="card bg-warning text-primary">
                        <div class="card-body">
                            <i class="fas fa-eye fa-3x"></i>
                            <h6 class="card-title">Visitas</h6>
                            <h2 class="lead">{{$total_views}}</h2>
                            <span class="card-link text-primary">Total de visitas</span>
                        </div>
                    </div>
                </div>
            </div>
            @if ($post_mais_lido)
            <div class="row mb-3">
                <div class="col-lg-12 col-sm-12">
                    <div class="card bg-info text-white">
                        <div class="card-body">
                            <article class="row">
                                <div class="col-xs-12 col-md-6 col-lg-3 mb-3 text-center">
                                    <a href="/post/{{$post_mais_lido->id}}" target="_blank" class="card-link text-white col-md-4">
                                        @if ($post_mais_lido->url_imagem)
                                            <img height="150" src="{{$post_mais_lido->url_imagem}}" class="" alt="{{$post_mais_lido->title}}">
                                        @else
                                            <img v-else height="150" src="/storage/posts/defaultthumbnail.png" class="" alt="{{$post_mais_lido->title}}">
                                        @endif
                                    </a>
                                </div>
                                <div class="cols-xs-12 col-md-6 col-lg-9">
                                    <h5 class="card-title">Post Mais lido</h5>
                                    <h2 class="lead">{{$post_mais_lido->views}} views</h2>
                                    <h2 class="p-2">
                                        <a href="/post/{{$post_mais_lido->id}}" target="_blank" class="card-link text-white">{{$post_mais_lido->title}}</a>
                                    </h2>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
