@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content p-1">
            <div class="list-group-item">
                <div class="d-flex">
                    <div class="mr-auto p-2">
                        <h2 class="display-8 titulo">Listar posts</h2>
                    </div>
                    <a href="{{ url('/admin/posts/create') }}">
                        <div class="p-2">
                            <button class="btn btn-outline-success btn-sm">
                                Cadastrar
                            </button>
                        </div>
                    </a>
                </div>
{{--                <div class="alert alert-success hide" role="alert">--}}
{{--                    Usuário apagado com sucesso!--}}
{{--                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
{{--                        <span aria-hidden="true">&times;</span>--}}
{{--                    </button>--}}
{{--                </div>--}}
                <div class="table-responsive">
                    <table id="dataTable" class="table table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Título</th>
                            <th class="d-none d-lg-table-cell">Data do Cadastro</th>
                            <th class="d-none d-sm-table-cell">Publicado</th>
                            <th class="text-center">Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                        <tr>
                            <th>{{ $post->id }}</th>
                            <td>{{ $post->title }}</td>
                            <td class="d-none d-sm-table-cell">{{ $post->created_at}}</td>
                            <td class="d-none d-lg-table-cell text-center">
                                @if($post->published)
                                    <i class="fas fa-check text-success"></i>
                                @else
                                    <i class="fa-solid fa-circle-xmark text-danger"></i>
                                @endif
                            </td>
                            <td class="text-center">
                                <span class="d-none d-md-block">
                                    <a href="{{ url('/admin/posts/edit/' . $post->id) }}" class="btn btn-outline-primary btn-sm">Editar</a>
                                    <a title="Apagar este registro?" href="#" data-toggle="modal" data-target="#apagarRegistro_{{$post->id}}" class="btn btn-outline-danger btn-sm">Apagar</a>
                                </span>
                                <div class="dropdown d-block d-md-none">
                                    <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Ações
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                                        <a class="dropdown-item" href="{{ url('/admin/posts/edit/' . $post->id) }}">Editar</a>
                                        <a title="Apagar este registro?" class="dropdown-item" href="#" data-toggle="modal" data-target="#apagarRegistro_{{$post->id}}">Apagar</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                    <p class="text-left">Exibindo {{ $posts->count() }} de {{ $posts->total() }}</p>
                    <nav aria-label="paginacao">
                        <ul class="pagination pagination-sm justify-content-center">
                            @foreach($paginacao as $key => $page)
                                @if($key===1)
                                <li class="page-item">
                                    <a class="page-link" href="{{ $page }}" tabindex="-1">Primeira</a>
                                </li>
                                @endif

                                @if($posts->currentPage() === $key)
                                    <li class="page-item active">
                                        <a class="page-link" href="#">{{$key}}</a>
                                    </li>
                                @else
                                    <li class="page-item"><a class="page-link" href="{{ $page }}">{{ $key }}</a></li>
                                @endif

                                @if($key === $posts->lastPage())
                                <li class="page-item">
                                    <a class="page-link" href="{{ $page }}">Última</a>
                                </li>
                                @endif
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        @foreach($posts as $post)
            <div class="modal fade" id="apagarRegistro_{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="apagarRegistroLabel{{$post->id}}" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger text-white">
                        <h5 class="modal-title" id="exampleModalLabel">EXCLUIR ITEM</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Tem certeza de que deseja excluir o item selecionado?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                        <a href="{{ url('/admin/posts/destroy/' . $post->id) }}" type="button" class="btn btn-danger">Apagar</a>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection
