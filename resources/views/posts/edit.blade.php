@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content p-1">
            <div class="list-group-item">
                <div class="d-flex">
                    <div class="mr-auto p-2">
                        <h2 class="display-8 titulo">Editar Post</h2>
                    </div>
                    <div class="p-2">
                            <span class="d-none d-md-block">
                                <a href="/admin/posts" class="btn btn-outline-info btn-sm">Listar</a>
                                <a href="{{ url('/admin/posts/destroy/' . $post->id) }}" class="btn btn-outline-danger btn-sm" data-toggle="modal" data-target="#apagarRegistro">Apagar</a>
                            </span>
                        <div class="dropdown d-block d-md-none">
                            <button class="btn btn-primary dropdown-toggle btn-sm" type="button" id="acoesListar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ações
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="acoesListar">
                                <a class="dropdown-item" href="/admin/posts">Listar</a>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#apagarRegistro">Apagar</a>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">Posts Edit</div>

                            <div class="card-body">
                                <form action="{{ url('/admin/posts/update/' . $post->id) }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="mb-5 form-group has-feedback{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="totle" class="text-muted">Title</label>
                                        <input id="title" type="text" value="{{ $post->title }}" name="title"
                                               class="form-control">
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="mb-5 form-group has-feedback{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label for="body" class="text-muted">Body</label>
                                        <textarea id="body" name="body" rows="10"
                                                  class="form-control">{{ $post->body }}</textarea>
                                        @if ($errors->has('body'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="mb-5 form-group has-feedback{{ $errors->has('tags') ? ' has-error' : '' }}">
                                        <label for="tags" class="text-muted">Tags</label>
                                        <select id="tags" name="tags[]" multiple class="form-control">
                                            @foreach(\App\Tag::all() as $tag)
                                                <option value="{{ $tag->id }}"
                                                        @if($post->has_tag($tag->id)) selected @endif>{{ $tag->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('tags'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="mb-5 form-group has-feedback{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <label for="body" class="text-muted">Adicionar imagem?</label>
                                        <input type="file" name="image">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif

                                        @if($post->url_imagem)
                                            <div class="col-md-12 mt-2">
                                                <figure>
                                                    <figcaption>Imagem cadastrada</figcaption>
                                                    <img src="{{ url("{$post->url_imagem}") }}" height="200" alt="{{ $post->title }}">
                                                </figure>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="mb-5 form-group has-feedback{{ $errors->has('published') ? ' has-error' : '' }}">
                                        <label for="totle" class="text-muted">Publicado?</label>

                                        <div class="form-check form-check-inline">
                                            <input checked="{{$post->published ? true : false}}" class="form-check-input" type="radio" name="published" id="published" value="1">
                                            <label for="published" class="form-check-label">Sim</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input {{$post->published ? '' : 'checked' }} class="form-check-input" type="radio" name="published" id="not_published" value="0">
                                            <label for="not_published" class="form-check-label">Não</label>
                                        </div>

                                        @if ($errors->has('published'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('published') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="apagarRegistro" tabindex="-1" role="dialog" aria-labelledby="apagarRegistroLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-danger text-white">
                        <h5 class="modal-title" id="exampleModalLabel">EXCLUIR ITEM</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Tem certeza de que deseja excluir o item selecionado?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>
                        <a title="Deseja apagar este registro?" type="button" class="btn btn-danger" href="{{ url('/admin/posts/destroy/' . $post->id) }}">Apagar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
