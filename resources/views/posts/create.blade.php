@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="content p-1">
            <div class="list-group-item">
                <div class="d-flex">
                    <div class="mr-auto p-2">
                        <h2 class="display-8 titulo">Cadastrar Post</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ url('/admin/posts/store') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="title" class="text-muted">Title</label>
                                <input id="title" type="text" name="title" class="form-control">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback{{ $errors->has('body') ? ' has-error' : '' }}">
                                <label for="body" class="text-muted">Body</label>
                                <textarea id="body" name="body" rows="10" class="form-control"></textarea>
                                @if ($errors->has('body'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label for="tags" class="text-muted">Tags</label>
                                <select id="tags" multiple="multiple" class="my-select form-select" name="tags[]">
                                    @foreach(\App\Tag::all() as $tag)
                                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('tags'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('tags') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group has-feedback{{ $errors->has('image') ? ' has-error' : '' }}">
                                <label for="body" class="text-muted">Adicionar imagem?</label>
                                <input type="file" name="image">
                                @if ($errors->has('image'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback{{ $errors->has('published') ? ' has-error' : '' }}">
                                <label for="title" class="text-muted">Publicar post?</label>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="published" id="published" value="1">
                                    <label for="published" class="form-check-label">Sim</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input checked="checked" class="form-check-input" type="radio" name="published" id="not_published" value="0">
                                    <label for="not_published" class="form-check-label">Não</label>
                                </div>

                                @if ($errors->has('published'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('published') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
@endsection
