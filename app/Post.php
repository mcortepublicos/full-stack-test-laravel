<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends ModelDefault
{
    protected $guarded = [];

    protected $fillable = [
        'title', 'body', 'url_imagem','published','views'
    ];

    protected $hidden = [];

    public function scopePublished($query) {
        return $query->whereNotNull('published');
    }

    public function has_tag($tag_id)
    {
        $rows = PostTag::where('tag_id', $tag_id)->where('post_id', $this->id)->get();

        if (count($rows) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function tags() {
        return $this->hasMany(PostTag::class, 'post_id')->with('tag');
    }

    public function userCreated() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function userUpdated() {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
