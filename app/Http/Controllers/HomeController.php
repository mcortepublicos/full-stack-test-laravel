<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // post publicados
        $publicados = Post::Published()->count();

        // post mais lido
        $post_mais_lido = Post::Published()->orderByDesc('views')->first();

        // post mais lido
        $total_views = Post::sum('views');

        // usuários registrados
        $users = User::count();

        return view('home', compact('publicados', 'users', 'post_mais_lido', 'total_views'));
    }
}
