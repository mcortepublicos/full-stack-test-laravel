<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostTag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // lista de posts com paginacao
        $posts = Post::orderByDesc('id')->paginate(10);
        $paginacao = $posts->getUrlRange(1, $posts->lastPage());

        return view('posts.index', compact('posts', 'paginacao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        DB::transaction(function () use ($request) {
            $post = Post::create([
                'title' => $request->title,
                'body' => $request->body,
                'published' => $request->published ? Carbon::now() : null,
            ]);

            if ($request->tags) {
                foreach ($request->tags as $tag_id) {
                    PostTag::insert([
                        'tag_id' => $tag_id,
                        'post_id' => $post->id
                    ]);
                }
            }

            // Define o valor default para a variável que contém o nome da imagem
            $nameFile = null;

            // Verifica se informou o arquivo e se é válido
            if ($request->hasFile('image') && $request->file('image')->isValid()) {

                // Define um aleatório para o arquivo baseado no timestamps atual
                $name = uniqid(date('HisYmd'));

                // Recupera a extensão do arquivo
                $extension = $request->image->extension();

                // Define finalmente o nome
                $nameFile = "{$name}.{$extension}";

                // Faz o upload:
                $upload = $request->image->storeAs("public/posts/{$post->id}/", $nameFile);
                // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

                // Verifica se NÃO deu certo o upload (Redireciona de volta)
                if ( !$upload )
                    return redirect()
                        ->back()
                        ->with('error', 'Falha ao fazer upload')
                        ->withInput();

                // atualiza o post com o valorda imagem
                $post->update([
                    'url_imagem' => "/storage/posts/{$post->id}/{$nameFile}",
                ]);
            }
        });

        return redirect('/admin/posts');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        $post = Post::find($id);

        if ($post) {
            DB::transaction(function () use ($request, $post) {
                // Define o valor default para a variável que contém o nome da imagem
                $nameFile = null;
                $tem_imagem = false;

                // Verifica se informou o arquivo e se é válido
                if ($request->hasFile('image') && $request->file('image')->isValid()) {

                    // Define um aleatório para o arquivo baseado no timestamps atual
                    $name = uniqid(date('HisYmd'));

                    // Recupera a extensão do arquivo
                    $extension = $request->image->extension();

                    // Define finalmente o nome
                    $nameFile = "{$name}.{$extension}";

                    // Faz o upload:
                    $upload = $request->image->storeAs("public/posts/{$post->id}/", $nameFile);
                    // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

                    // Verifica se NÃO deu certo o upload (Redireciona de volta)
                    if ( !$upload )
                        return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();

                    // controle se tem iamgem
                    $tem_imagem = true;
                }

                $post->update([
                    'title' => $request->title,
                    'body' => $request->body,
                    'published' => $request->published ? Carbon::now() : null,
                    'url_imagem' => $tem_imagem ? "/storage/posts/{$post->id}/{$nameFile}" : $post->url_imagem,
                ]);

                PostTag::where('post_id', $post->id)->delete();

                if ($request->tags) {
                    foreach ($request->tags as $tag_id) {
                        PostTag::insert([
                            'tag_id' => $tag_id,
                            'post_id' => $post->id
                        ]);
                    }
                }
            });
        }

        return redirect('/admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $post = Post::find($id);

        if ($post) {
            DB::transaction(function () use ($post) {
                $post->delete();
                PostTag::where('post_id', $post->id)->delete();
            });
        }

        return redirect('/admin/posts');
    }
}
