<?php

namespace App\Http\Controllers\Api;

use App\Post;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        // busca posts publicados com relacionamentos de tags e usuários
        $posts = Post::with('tags')
            ->Published()
            ->with(['tags', 'userCreated', 'userUpdated'])
            ->orderByDesc('published')
            ->limit($limit ?? 5 )
            ->get();

        return response()->json($posts);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function lastposts($limit)
    {
        // busca posts publicados com relacionamentos de tags e usuários
        $posts = Post::Published()
            ->orderByDesc('published')
            ->limit($limit ?? 5 )
            ->get();

        return response()->json($posts);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function mostreadposts($limit)
    {
        // busca posts publicados com relacionamentos de tags e usuários
        $posts = Post::Published()
            ->orderByDesc('views')
            ->limit($limit ?? 5 )
            ->get();

        return response()->json($posts);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function view($id)
    {
        // busca post id com relacionamentos de tags e usuários
        $post = Post::where('id', $id)
            ->with(['tags', 'userCreated', 'userUpdated'])
            ->first();

        if ($post) {
            $post->update([
                'views' => $post->views + 1
            ]);
        }

        return response()->json($post);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function show($id)
    {
        // busca post id com relacionamentos de tags e usuários
        $post = Post::where('id', $id)
            ->with(['tags', 'userCreated', 'userUpdated'])
            ->first();

        return view('posts.show', compact('post'));
    }
}
