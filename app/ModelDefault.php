<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ModelDefault extends Model {

//    use SoftDeletes;

    protected $dates = [
        'created_at', 'updated_at'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'created_by', 'updated_by'
    ];

    protected static function boot() {

        self::creating(function ($model) {
            $model->created_by = self::getUserIdLogged();
        });

        self::updating(function ($model) {
            $model->updated_by = self::getUserIdLogged();
        });

        parent::boot();
    }

    /**
     * Se existe usuário logado retorna o id
     * @return string
     */
    private static function getUserIdLogged() {
        return Auth::check() ? Auth::user()->id : null;
    }
}
