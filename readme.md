### Sobre o projeto
Gerenciador de postagens de conteúdo para um blog. 
Mais um blog listando os conteúdos publicados pelo gerenciador.

### Orientações

##### Faça um clone deste repositório

git clone git@bitbucket.org:mcortepublicos/full-stack-test-laravel.git full-stack-test-laravel

Bash
```bash
$ cd full-stack-test-laravel
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ touch database/database.sqlite
$ php artisan migrate --seed
$ npm install
$ npm run dev
$ php artisan storage:link
$ php artisan serve 
```
email: admin@test.com | senha: 123456
 
##### Notas de desenvolvimento
1. Desenvolvimento de blog com listagem de posts e página que exibe o post específico. 
2. No admin são exibidas as seguintes informações 
   1. Dashboard
      1. Total de usuários cadastrados
      2. Totoal de posts publicados com acesso ao cadastro de posts
      3. Total de visualizações dos posts
      4. Post mais visualizado com detalhes e link para acesso ao mesmo

   2. Cadastro de Post
         1. Adicionada ao cadastro a imagem destaque
         2. Campo published (sim/não)
         3. Paginação na listagem dos registros 
      
3. No blog são exibidas as informações
   1. Imagem destaque
   2. Título do post
   3. Informações data e usuário que criou o post
   4. Tags
   
